#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_play, diplomacy_write, diplomacy_solve

# -----------
# Testdiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]')
            
    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]')
    
    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB Madrid\nC [dead]\nD Paris')

    def test_solve_4(self): # Everyone attacks (everyone moves in a circle)
        r = StringIO('A Madrid Move Barcelona\nB Barcelona Move Paris\nC Paris Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Barcelona\nB Paris\nC Madrid')
    
    def test_solve_5(self): # A big battle where no one wins
        r = StringIO('A Madrid Move Barcelona\nB Paris Move Barcelona\nC Paris1 Support A\nD Austin Support B\nE Austin1 Support A\nF Austin2 Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC Paris1\nD Austin\nE Austin1\nF Austin2')
    
    def test_solve_6(self): # A big battle where someone wins
        r = StringIO('A Madrid Move Barcelona\nB Paris Move Barcelona\nC Paris1 Support A\nD Austin Support B\nE Austin1 Support A\nF Austin2 Support B\nG Austin3 Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Barcelona\nB [dead]\nC Paris1\nD Austin\nE Austin1\nF Austin2\nG Austin3')
    
    def test_solve_7(self): # No one does anything
        r = StringIO('A Madrid Hold\nB Paris Hold\nC Paris1 Hold\nD Austin Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Madrid\nB Paris\nC Paris1\nD Austin')
    
    def test_solve_8(self): # Everyone attacks the same city
        r = StringIO('A Madrid Move Boston\nB Paris Move Boston\nC Paris1 Move Boston\nD Austin Move Boston')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]')

    def test_solve_9(self):  # All cities hold
        r = StringIO('A Madrid Hold\nB Boston Hold\nC Austin Support A\nD Paris Move Madrid')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A Madrid\nB Boston\nC Austin\nD [dead]')
# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()
